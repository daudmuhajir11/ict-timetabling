class SOLUTION: 
    def __init__(self, home, away, slot): 
        self.home = home
        self.away = away
        self.slot = slot

class TEAMS: 
    def __init__(self, id, league, name): 
        self.id = id
        self.league = league
        self.name = name

class SLOTS: 
    def __init__(self, id, name): 
        self.id = id
        self.name = name

class CA1: 
    def __init__(self, max, min, mode, penalty, slots, teams, type): 
        self.max = max
        self.min = min
        self.mode = mode
        self.penalty = penalty
        self.slots = slots
        self.teams = teams
        self.type = type

class CA2:
    def __init__(self, max, min, mode1, mode2, penalty, slots, teams1, teams2, type): 
        self.max = max
        self.min = min
        self.mode1 = mode1
        self.mode2 = mode2
        self.penalty = penalty
        self.slots = slots
        self.teams1 = teams1
        self.teams2 = teams2
        self.type = type

class CA3: 
    def __init__(self, intp, max, min, mode1, mode2, penalty, teams1, teams2, type): 
        self.intp = intp
        self.max = max
        self.min = min
        self.mode1 = mode1
        self.mode2 = mode2
        self.penalty = penalty
        self.teams1 = teams1
        self.teams2 = teams2
        self.type = type

class CA4: 
    def __init__(self, max, min, mode1, mode2, penalty, slots, teams1, teams2, type): 
        self.max = max
        self.min = min
        self.mode1 = mode1
        self.mode2 = mode2
        self.penalty = penalty
        self.slots = slots
        self.teams1 = teams1
        self.teams2 = teams2
        self.type = type

class GA1: 
    def __init__(self, max, meetings, min, penalty, slots, type): 
        self.max = max
        self.meetings = meetings
        self.min = min
        self.penalty = penalty
        self.slots = slots
        self.type = type

class BR1: 
    def __init__(self, intp, mode1, mode2, penalty, slots, teams, type): 
        self.intp = intp
        self.mode1 = mode1
        self.mode2 = mode2
        self.penalty = penalty
        self.slots = slots
        self.teams = teams
        self.type = type

class BR2: 
    def __init__(self, intp, homeMode, mode2, penalty, slots, teams, type): 
        self.intp = intp
        self.homeMode = homeMode
        self.mode2 = mode2
        self.penalty = penalty
        self.slots = slots
        self.teams = teams
        self.type = type

class FA2: 
    def __init__(self, intp, mode, penalty, slots, teams, type): 
        self.intp = intp
        self.mode = mode
        self.penalty = penalty
        self.slots = slots
        self.teams = teams
        self.type = type

class SE1: 
    def __init__(self, mode1, min, penalty, teams, type): 
        self.mode1 = mode1
        self.min = min
        self.penalty = penalty
        self.teams = teams
        self.type = type