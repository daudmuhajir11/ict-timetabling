from xml.dom import minidom
from xml.dom.minidom import Node
import random
from constraint_class import SOLUTION, CA1, CA2, CA3, CA4, GA1, BR1, BR2, FA2, SE1
from read_sports_timetabling_instance import handle_teams, handle_slots, handle_ca1, handle_ca2, handle_ca3, handle_ca4, handle_ga1, handle_br1, handle_br2, handle_fa2, handle_se1
from object_sports_timetabling_instance import object_teams, object_slots, object_ca1
import time

# def create_balanced_round_robin(players):
def create_base_solution(players, slots):
    """ Create a schedule for the players in the list and return it"""
    s = []
    if len(players) % 2 == 1: players = players + [None]
    # manipulate map (array of indexes for list) instead of list itself
    # this takes advantage of even/odd indexes to determine home vs. away
    n = len(players)
    map = list(range(n))
    mid = n // 2
    half = len(slots)/2
    for i in range(n-1):
        l1 = map[:mid]
        l2 = map[mid:]
        l2.reverse()
        round = []
        for j in range(mid):
            t1 = players[l1[j]]
            t2 = players[l2[j]]
            if j == 0 and i % 2 == 1:
                # flip the first match only, every other round
                # (this is because the first match always involves the last player in the list)
                round.append((t2, t1, i))
            else:
                round.append((t1, t2, i))
        s.append(round)
        # rotate list by n/2, leaving last element at the end
        map = map[mid:-1] + map[:mid] + map[-1:]
    
    # double round robin
    for i in range(n-1):
        l1 = map[:mid]
        l2 = map[mid:]
        l2.reverse()
        round = []
        for j in range(mid):
            t1 = players[l1[j]]
            t2 = players[l2[j]]
            if j == 0 and i % 2 == 1:
                # flip the first match only, every other round
                # (this is because the first match always involves the last player in the list)
                round.append((t1, t2, int(i+half)))
            else:
                round.append((t2, t1, int(i+half)))
        s.append(round)
        # rotate list by n/2, leaving last element at the end
        map = map[mid:-1] + map[:mid] + map[-1:]

    return s

def write_solution_xml(solution,path):
    doc = minidom.Document()
    root = doc.createElement("Solution")
    metadata = doc.createElement('MetaData')
    root.appendChild(metadata)
    games = doc.createElement("Games")
    root.appendChild(games)
    doc.appendChild(root)
    for round in solution:
        for match in round:
            # Create Element
            tempChild = doc.createElement('ScheduledMatch')
            games.appendChild(tempChild)
            # Write Text
            tempChild.setAttribute("home", str(match[0]))
            tempChild.setAttribute("away", str(match[1]))
            tempChild.setAttribute("slot", str(match[2]))

            doc.writexml( open('solution/'+path, 'w'),
                indent="  ",
                addindent="  ",
                newl='\n')
    
    doc.unlink()

def count_ca1(solution, constraint):
    p_ca1 = [0, 0]
    dev = 0

    for i in range(len(constraint)):
        if(constraint[i].type == 'HARD'):
            for j in range(len(constraint[i].teams)):
                temp = 0
                for k in range(len(constraint[i].slots)):
                    for l in range(len(solution[int(constraint[i].slots[k])])):
                        if(constraint[i].mode != 'A'):
                            if(int(solution[int(constraint[i].slots[k])][l][0]) == int(constraint[i].teams[j])):
                                temp = temp + 1
                        if(constraint[i].mode != 'H'):
                            if(int(solution[int(constraint[i].slots[k])][l][1]) == int(constraint[i].teams[j])):
                                temp = temp + 1
                if (temp > int(constraint[i].max)):
                    dev = dev + ((temp - int(constraint[i].max)) * int(constraint[i].penalty))
                if temp < int(constraint[i].min):
                    dev = dev + ((int(constraint[i].min) - temp) * int(constraint[i].penalty))

    p_ca1[0] = dev
    dev = 0
    for i in range(len(constraint)):
        if(constraint[i].type == 'SOFT'):
            for j in range(len(constraint[i].teams)):
                temp = 0
                for k in range(len(constraint[i].slots)):
                    for l in range(len(solution[int(constraint[i].slots[k])])):
                        if(constraint[i].mode != 'A'):
                            if(int(solution[int(constraint[i].slots[k])][l][0]) == int(constraint[i].teams[j])):
                                temp = temp + 1
                        if(constraint[i].mode != 'H'):
                            if(int(solution[int(constraint[i].slots[k])][l][1]) == int(constraint[i].teams[j])):
                                temp = temp + 1
                if (temp > int(constraint[i].max)):
                    dev = dev + ((temp - int(constraint[i].max)) * int(constraint[i].penalty))
                if temp < int(constraint[i].min):
                    dev = dev + ((int(constraint[i].min) - temp) * int(constraint[i].penalty))
    
    p_ca1[1] = dev
    
    return p_ca1

def count_ca2(solution, constraint):
    p_ca2 = [0, 0]
    dev = 0

    for i in range(len(constraint)):
        if(constraint[i].type == 'HARD'):
            for j in range(len(constraint[i].teams1)):
                temp = 0
                for k in range(len(constraint[i].slots)):
                    for l in range(len(solution[int(constraint[i].slots[k])])):
                        for m in range(len(constraint[i].teams2)):
                            if(constraint[i].mode1 != 'A'):
                                if(int(solution[int(constraint[i].slots[k])][l][0]) == int(constraint[i].teams1[j]) and
                                    int(solution[int(constraint[i].slots[k])][l][1]) == int(constraint[i].teams2[m])):
                                    temp = temp + 1
                            if(constraint[i].mode1 != 'H'):
                                if(int(solution[int(constraint[i].slots[k])][l][1]) == int(constraint[i].teams1[j]) and
                                    int(solution[int(constraint[i].slots[k])][l][0]) == int(constraint[i].teams2[m])):
                                    temp = temp + 1
                if temp > int(constraint[i].max):
                    dev = dev + ((temp - int(constraint[i].max)) * int(constraint[i].penalty))
                if temp < int(constraint[i].min):
                    dev = dev + ((int(constraint[i].min) - temp) * int(constraint[i].penalty))

    p_ca2[0] = dev
    dev = 0

    for i in range(len(constraint)):
        if(constraint[i].type == 'SOFT'):
            for j in range(len(constraint[i].teams1)):
                temp = 0
                for k in range(len(constraint[i].slots)):
                    for l in range(len(solution[int(constraint[i].slots[k])])):
                        for m in range(len(constraint[i].teams2)):
                            if(constraint[i].mode1 != 'A'):
                                if(int(solution[int(constraint[i].slots[k][l][0])]) == int(constraint[i].teams1[j]) and
                                    int(solution[int(constraint[i].slots[k][l][1])]) == int(constraint[i].teams2[m])):
                                    temp = temp + 1
                            if(constraint[i].mode1 != 'H'):
                                if(int(solution[int(constraint[i].slots[k][l][1])]) == int(constraint[i].teams1[j]) and
                                    int(solution[int(constraint[i].slots[k][l][0])]) == int(constraint[i].teams2[m])):
                                    temp = temp + 1
                if temp > int(constraint[i].max):
                    dev = dev + ((temp - int(constraint[i].max)) * int(constraint[i].penalty))
                if temp < int(constraint[i].min):
                    dev = dev + ((int(constraint[i].min) - temp) * int(constraint[i].penalty))

    p_ca2[1] = dev

    return p_ca2

def count_ca3(solution, constraint, slot_size):
    p_ca3 = [0, 0]
    dev = 0

    for i in range(len(constraint)):
        if(constraint[i].type == 'HARD'):
            for j in range(len(constraint[i].teams1)):
                temp = 0
                for k in range((slot_size-int(constraint[i].intp))+1):
                    temp2 = 0
                    for l in range(len(constraint[i].teams2)):
                        for m in range(k,k+int(constraint[i].intp)):
                            for n in range(len(solution[m])):
                                if(constraint[i].mode1 != 'A'):
                                    if(int(solution[m][n][0]) == int(constraint[i].teams1[j]) and
                                        int(solution[m][n][1]) == int(constraint[i].teams2[l])):
                                        temp2 = temp2 + 1
                                if(constraint[i].mode1 != 'H'):
                                    if(int(solution[m][n][1]) == int(constraint[i].teams1[j]) and
                                        int(solution[m][n][0]) == int(constraint[i].teams2[l])):
                                        temp2 = temp2 + 1
                    if temp2 < int(constraint[i].min):
                        temp = temp + (int(constraint[i].min) - temp2)
                    if temp2 > int(constraint[i].max):
                        temp = temp + (temp2 - int(constraint[i].max))
                dev = dev + (temp * int(constraint[i].penalty))

    p_ca3[0] = dev
    dev = 0

    for i in range(len(constraint)):
        if(constraint[i].type == 'SOFT'):
            for j in range(len(constraint[i].teams1)):
                temp = 0
                for k in range((slot_size-int(constraint[i].intp))+1):
                    temp2 = 0
                    for l in range(len(constraint[i].teams2)):
                        for m in range(k,k+int(constraint[i].intp)):
                            for n in range(len(solution[m])):
                                if(constraint[i].mode1 != 'A'):
                                    if(int(solution[m][n][0]) == int(constraint[i].teams1[j]) and
                                        int(solution[m][n][1]) == int(constraint[i].teams2[l])):
                                        temp2 = temp2 + 1
                                if(constraint[i].mode1 != 'H'):
                                    if(int(solution[m][n][1]) == int(constraint[i].teams1[j]) and
                                        int(solution[m][n][0]) == int(constraint[i].teams2[l])):
                                        temp2 = temp2 + 1
                    if temp2 < int(constraint[i].min):
                        temp = temp + (int(constraint[i].min) - temp2)
                    if temp2 > int(constraint[i].max):
                        temp = temp + (temp2 - int(constraint[i].max))
                dev = dev + (temp * int(constraint[i].penalty))

    p_ca3[1] = dev
    return p_ca3

def count_ca4(solution, constraint):
    p_ca4 = [0, 0]
    dev = 0

    for i in range(len(constraint)):
        temp = 0
        if(constraint[i].type == 'HARD'):
            for j in range(len(constraint[i].slots)):
                temp2 = 0
                for k in range(len(constraint[i].teams1)):
                    for l in range(len(constraint[i].teams2)):
                        for m in range(len(solution[int(constraint[i].slots[j])])):
                            if(constraint[i].mode1 != 'A'):
                                if(int(solution[int(constraint[i].slots[j])][m][0]) == int(constraint[i].teams1[k]) and
                                    int(solution[int(constraint[i].slots[j])][m][1]) == int(constraint[i].teams2[l])):
                                    if(constraint[i].mode2 == 'EVERY'):
                                        temp2 = temp2 + 1
                                    else:
                                        temp = temp + 1
                            if(constraint[i].mode1 != 'H'):
                                if(int(solution[int(constraint[i].slots[j])][m][1]) == int(constraint[i].teams1[k]) and
                                    int(solution[int(constraint[i].slots[j])][m][0]) == int(constraint[i].teams2[l])):
                                    if(constraint[i].mode2 == 'EVERY'):
                                        temp2 = temp2 + 1
                                    else:
                                        temp = temp + 1
                if (temp2 > int(constraint[i].max)) and (constraint[i].mode2 == 'EVERY'):
                    dev = dev + ((temp2 - int(constraint[i].max)) * int(constraint[i].penalty))
                if (temp2 < int(constraint[i].min)) and (constraint[i].mode2 == 'EVERY'):
                    dev = dev + ((int(constraint[i].min) - temp2) * int(constraint[i].penalty))
            if (temp > int(constraint[i].max)) and (constraint[i].mode2 == 'GLOBAL'):
                dev = dev + ((temp-int(constraint[i].max)) * int(constraint[i].penalty))
            if (temp < int(constraint[i].min)) and (constraint[i].mode2 == 'GLOBAL'):
                dev = dev + ((int(constraint[i].min)-temp) * int(constraint[i].penalty))

    p_ca4[0] = dev
    dev = 0

    for i in range(len(constraint)):
        temp = 0
        if(constraint[i].type == 'SOFT'):
            for j in range(len(constraint[i].slots)):
                temp2 = 0
                for k in range(len(constraint[i].teams1)):
                    for l in range(len(constraint[i].teams2)):
                        for m in range(len(solution[int(constraint[i].slots[j])])):
                            if(constraint[i].mode1 != 'A'):
                                if(int(solution[int(constraint[i].slots[j])][m][0]) == int(constraint[i].teams1[k]) and
                                    int(solution[int(constraint[i].slots[j])][m][1]) == int(constraint[i].teams2[l])):
                                    if(constraint[i].mode2 == 'EVERY'):
                                        temp2 = temp2 + 1
                                    else:
                                        temp = temp + 1
                            if(constraint[i].mode1 != 'H'):
                                if(int(solution[int(constraint[i].slots[j])][m][1]) == int(constraint[i].teams1[k]) and
                                    int(solution[int(constraint[i].slots[j])][m][0]) == int(constraint[i].teams2[l])):
                                    if(constraint[i].mode2 == 'EVERY'):
                                        temp2 = temp2 + 1
                                    else:
                                        temp = temp + 1
                if (temp2 > int(constraint[i].max)) and (constraint[i].mode2 == 'EVERY'):
                    dev = dev + ((temp2 - int(constraint[i].max)) * int(constraint[i].penalty))
                if (temp2 < int(constraint[i].min)) and (constraint[i].mode2 == 'EVERY'):
                    dev = dev + ((int(constraint[i].min) - temp2) * int(constraint[i].penalty))
            if (temp > int(constraint[i].max)) and (constraint[i].mode2 == 'GLOBAL'):
                dev = dev + ((temp-int(constraint[i].max)) * int(constraint[i].penalty))
            if (temp < int(constraint[i].min)) and (constraint[i].mode2 == 'GLOBAL'):
                dev = dev + ((int(constraint[i].min)-temp) * int(constraint[i].penalty))

    p_ca4[1] = dev
    return p_ca4

def team_swap(solution, team_size):
    solution = list(solution)
    rand1 = random.randint(0, team_size-1)
    rand2 = random.randint(0, team_size-1)
    while rand2 == rand1:
        rand2 = random.randint(0, team_size-1)
    for i in range(len(solution)):
        for j in range(len(solution[i])):
            if(int(solution[i][j][0])==rand1):
                if(int(solution[i][j][1])!=rand2):
                    solution[i][j] = list(solution[i][j])
                    solution[i][j][0] = int(rand2)
                    solution[i][j] = tuple(solution[i][j])
                    continue
            if(int(solution[i][j][0])==rand2):
                if(int(solution[i][j][1])!=rand1):
                    solution[i][j] = list(solution[i][j])
                    solution[i][j][0] = int(rand1)
                    solution[i][j] = tuple(solution[i][j])
                    continue
            if(int(solution[i][j][1])==rand1):
                if(int(solution[i][j][0])!=rand2):
                    solution[i][j] = list(solution[i][j])
                    solution[i][j][1] = int(rand2)
                    solution[i][j] = tuple(solution[i][j])
                    continue
            if(int(solution[i][j][1])==rand2):
                if(int(solution[i][j][0])!=rand1):
                    solution[i][j] = list(solution[i][j])
                    solution[i][j][1] = int(rand1)
                    solution[i][j] = tuple(solution[i][j])
                    continue
    solution_temp = [0] * 10
    for s in range(len(solution)):
        if str(solution[s][0][2]) == '0':
            solution_temp[0] = solution[s]
        if str(solution[s][0][2]) == '1':
            solution_temp[1] = solution[s]
        if str(solution[s][0][2]) == '2':
            solution_temp[2] = solution[s]
        if str(solution[s][0][2]) == '3':
            solution_temp[3] = solution[s]
        if str(solution[s][0][2]) == '4':
            solution_temp[4] = solution[s]
        if str(solution[s][0][2]) == '5':
            solution_temp[5] = solution[s]
        if str(solution[s][0][2]) == '6':
            solution_temp[6] = solution[s]
        if str(solution[s][0][2]) == '7':
            solution_temp[7] = solution[s]
        if str(solution[s][0][2]) == '8':
            solution_temp[8] = solution[s]
        if str(solution[s][0][2]) == '9':
            solution_temp[9] = solution[s]
    return solution_temp

def swap_home(solution, team_size):
    solution = list(solution)
    rand1 = random.randint(0, team_size-1)
    rand2 = random.randint(0, team_size-1)
    while rand2 == rand1:
        rand2 = random.randint(0, team_size-1)
    for i in range(len(solution)):
        for j in range(len(solution[i])):
            if(int(solution[i][j][0])==rand1 and int(solution[i][j][1])==rand2):
                solution[i][j] = list(solution[i][j])
                solution[i][j][0] = int(rand2)
                solution[i][j][1] = int(rand1)
                solution[i][j] = tuple(solution[i][j])
                continue
            if(int(solution[i][j][1])==rand1 and int(solution[i][j][0])==rand2):
                solution[i][j] = list(solution[i][j])
                solution[i][j][0] = int(rand1)
                solution[i][j][1] = int(rand2)
                solution[i][j] = tuple(solution[i][j])
                continue
    solution_temp = [0] * 10
    for s in range(len(solution)):
        if str(solution[s][0][2]) == '0':
            solution_temp[0] = solution[s]
        if str(solution[s][0][2]) == '1':
            solution_temp[1] = solution[s]
        if str(solution[s][0][2]) == '2':
            solution_temp[2] = solution[s]
        if str(solution[s][0][2]) == '3':
            solution_temp[3] = solution[s]
        if str(solution[s][0][2]) == '4':
            solution_temp[4] = solution[s]
        if str(solution[s][0][2]) == '5':
            solution_temp[5] = solution[s]
        if str(solution[s][0][2]) == '6':
            solution_temp[6] = solution[s]
        if str(solution[s][0][2]) == '7':
            solution_temp[7] = solution[s]
        if str(solution[s][0][2]) == '8':
            solution_temp[8] = solution[s]
        if str(solution[s][0][2]) == '9':
            solution_temp[9] = solution[s]
    return solution_temp

def round_swap(solution, team_size, slot_size):
    rand1 = random.randint(0, slot_size-1)
    rand2 = random.randint(0, slot_size-1)
    while rand2 == rand1:
        rand2 = random.randint(0, slot_size-1)
    for i in range(int(team_size/2)):
        solution[rand1].append((solution[rand1][i][0],solution[rand1][i][1],solution[rand2][i][2]))
        solution[rand2].append((solution[rand2][i][0],solution[rand2][i][1],solution[rand1][i][2]))
    for i in range(int(team_size/2)):
        solution[rand1].pop(0)
        solution[rand2].pop(0)
    solution_temp = [0] * 10
    for s in range(len(solution)):
        if str(solution[s][0][2]) == '0':
            solution_temp[0] = solution[s]
        if str(solution[s][0][2]) == '1':
            solution_temp[1] = solution[s]
        if str(solution[s][0][2]) == '2':
            solution_temp[2] = solution[s]
        if str(solution[s][0][2]) == '3':
            solution_temp[3] = solution[s]
        if str(solution[s][0][2]) == '4':
            solution_temp[4] = solution[s]
        if str(solution[s][0][2]) == '5':
            solution_temp[5] = solution[s]
        if str(solution[s][0][2]) == '6':
            solution_temp[6] = solution[s]
        if str(solution[s][0][2]) == '7':
            solution_temp[7] = solution[s]
        if str(solution[s][0][2]) == '8':
            solution_temp[8] = solution[s]
        if str(solution[s][0][2]) == '9':
            solution_temp[9] = solution[s]
    return solution_temp

def round_3swap(solution, team_size, slot_size):
    rand1 = random.randint(0, slot_size-1)
    rand2 = random.randint(0, slot_size-1)
    rand3 = random.randint(0, slot_size-1)
    while rand2 == rand1:
        rand2 = random.randint(0, slot_size-1)
    while rand3 == rand1 or rand3 == rand2:
        rand3 = random.randint(0, slot_size-1)
    for i in range(int(team_size/2)):
        solution[rand1].append((solution[rand1][i][0],solution[rand1][i][1],solution[rand2][i][2]))
        solution[rand2].append((solution[rand2][i][0],solution[rand2][i][1],solution[rand3][i][2]))
        solution[rand3].append((solution[rand3][i][0],solution[rand3][i][1],solution[rand1][i][2]))
    for i in range(int(team_size/2)):
        solution[rand1].pop(0)
        solution[rand2].pop(0)
        solution[rand3].pop(0)
    solution_temp = [0] * 10
    for s in range(len(solution)):
        if str(solution[s][0][2]) == '0':
            solution_temp[0] = solution[s]
        if str(solution[s][0][2]) == '1':
            solution_temp[1] = solution[s]
        if str(solution[s][0][2]) == '2':
            solution_temp[2] = solution[s]
        if str(solution[s][0][2]) == '3':
            solution_temp[3] = solution[s]
        if str(solution[s][0][2]) == '4':
            solution_temp[4] = solution[s]
        if str(solution[s][0][2]) == '5':
            solution_temp[5] = solution[s]
        if str(solution[s][0][2]) == '6':
            solution_temp[6] = solution[s]
        if str(solution[s][0][2]) == '7':
            solution_temp[7] = solution[s]
        if str(solution[s][0][2]) == '8':
            solution_temp[8] = solution[s]
        if str(solution[s][0][2]) == '9':
            solution_temp[9] = solution[s]
    return solution_temp

def count_penalty(mydoc, solution, slot_size, debug):
    if debug == 1:
        print(solution)
    
    p = [0,0]
    # constraint ca1
    ca1 = handle_ca1(mydoc,0)
    p_ca1 = count_ca1(solution, ca1)
    p[0] = p[0] + p_ca1[0]
    p[1] = p[1] + p_ca1[1]
    if debug == 1:
        print(p_ca1)

    # constraint ca2
    ca2 = handle_ca2(mydoc,0)
    p_ca2 = count_ca2(solution, ca2)
    p[0] = p[0] + p_ca2[0]
    p[1] = p[1] + p_ca2[1]
    if debug == 1:
        print(p_ca2)

    # constraint ca3
    ca3 = handle_ca3(mydoc,0)
    p_ca3 = count_ca3(solution, ca3,slot_size)
    p[0] = p[0] + p_ca3[0]
    p[1] = p[1] + p_ca3[1]
    if debug == 1:
        print(p_ca3)

    # constraint ca4
    ca4 = handle_ca4(mydoc,0)
    p_ca4 = count_ca4(solution, ca4)
    p[0] = p[0] + p_ca4[0]
    p[1] = p[1] + p_ca4[1]
    if debug == 1:
        print(p_ca4)

    return p

def main(set, data):
    start_time = time.time()
    # parse an xml file by name
    # mydoc = minidom.parse('TestInstances_V3/TestInstanceDemo.xml')
    array_set = ['TestInstances_V3','EarlyInstances_V3','MiddleInstances','LateInstances']
    array_set_name = ['Test','Early','Middle','Late']
    path = array_set[set]+'/ITC2021_'+array_set_name[set]+'_'+str(data)+'.xml'
    # path = 'TestInstances_V3/TestInstanceDemo.xml'
    mydoc = minidom.parse(path)

    # mengembalikan array teams dan slots
    teams = handle_teams(mydoc,0)
    slots = handle_slots(mydoc,0)

    # mengembalikan base solution double round robin
    solution = create_base_solution(teams, slots)
    # print(count_penalty(mydoc,solution,len(slots)))
    # write_solution_xml(solution,path)
    # solution_temp = team_swap(solution,len(teams))
    # solution_temp = round_3swap(solution,len(teams),len(slots))
    # return print('selesai')
    # print(count_penalty(mydoc,solution_temp,len(slots),1))
    # write_solution_xml(solution_temp,path)
    # return print('selesai')
    # print("\n".join(['{} vs. {} slot. {}' .format(m[0], m[1], m[2]) for round in solution for m in round]))
    # write_solution_xml(solution,path)
    penalty = count_penalty(mydoc,solution,len(slots),0)
    fitness = [penalty[0]] * 100
    i = 0
    tabu = []
    while penalty[0] > 26:
        rand = random.randint(0, 3)
        while rand in tabu:
            rand = random.randint(0, 3)
        if(rand == 0):
            sol_temp = team_swap(solution,len(teams))
            print('nol')
        if(rand == 1):
            sol_temp = swap_home(solution,len(teams))
            print('satu')
        if(rand == 2):
            sol_temp = round_swap(solution,len(teams),len(slots))
            print('dua')
        if(rand == 3):
            sol_temp = round_3swap(solution,len(teams),len(slots))
            print('tiga')
        penalty_temp =  count_penalty(mydoc,sol_temp,len(slots),0)
        # if(penalty_temp[0] >= penalty[0]):
        #     iddle = iddle + 1
        #     tabu.append(rand)
        #     if(len(tabu)>1):
        #         tabu.pop(0)
        # else:
        #     iddle = 0
        # if(penalty_temp[0] < fitness[i%100] or penalty_temp[0] <= penalty[0] or iddle > 500):
        #     solution = sol_temp
        #     penalty = penalty_temp
        # if(penalty[0] < fitness[i%100]):
        #     fitness[i%100] = penalty[0]
        if(penalty_temp[0] <= fitness[i%100]):
            solution = sol_temp
            penalty = penalty_temp
            fitness[i%100] = penalty_temp[0]
        else:
            print('masuk tabu')
            tabu.append(rand)
            if(len(tabu)>3):
                tabu.pop(0)
            
        i = i + 1
        print('iterasi ke ' + str(i) + ' ' + str(penalty))
    
    write_solution_xml(solution,path)
    print("---- penalty ---")
    print(count_penalty(mydoc,solution,len(slots),1))
    print("--- %s seconds ---" % (time.time() - start_time))
    return print('selesai')

    # solution = base_solution(mydoc)
    # write_solution_xml(solution)

    # return print('selesai')

    # mengambil list constraint yang ada
    ca = mydoc.getElementsByTagName('CapacityConstraints')
    ga = mydoc.getElementsByTagName('GameConstraints')
    br = mydoc.getElementsByTagName('BreakConstraints')
    fa = mydoc.getElementsByTagName('FairnessConstraints')
    se = mydoc.getElementsByTagName('SeparationConstraints')
    list_ca = []
    list_ga = []
    list_br = []
    list_fa = []
    list_se = []
    for elem in ca:
        for x in elem.childNodes:
            if x.nodeType == Node.ELEMENT_NODE:
                list_ca.append(x.tagName)
    for elem in ga:
        for x in elem.childNodes:
            if x.nodeType == Node.ELEMENT_NODE:
                list_ga.append(x.tagName)
    for elem in br:
        for x in elem.childNodes:
            if x.nodeType == Node.ELEMENT_NODE:
                list_br.append(x.tagName)
    for elem in fa:
        for x in elem.childNodes:
            if x.nodeType == Node.ELEMENT_NODE:
                list_fa.append(x.tagName)
    for elem in se:
        for x in elem.childNodes:
            if x.nodeType == Node.ELEMENT_NODE:
                list_se.append(x.tagName)
    
    # menampilkan informasi team
    handle_teams(mydoc)
    print('\n')
    # menampilkan informasi slot
    handle_slots(mydoc)
    print('\n')
    # menampilkan informasi constraints
    if 'CA1' in list_ca:
        handle_ca1(mydoc)
        print('\n')
    if 'CA2' in list_ca:
        handle_ca2(mydoc)
        print('\n')
    if 'CA3' in list_ca:
        handle_ca3(mydoc)
        print('\n')
    if 'CA4' in list_ca:
        handle_ca4(mydoc)
        print('\n')
    if 'GA1' in list_ga:
        handle_ga1(mydoc)
        print('\n')
    if 'BR1' in list_br:
        handle_br1(mydoc)
        print('\n')
    if 'BR2' in list_br:
        handle_br2(mydoc)
        print('\n')
    if 'FA2' in list_fa:
        handle_fa2(mydoc)
        print('\n')
    if 'SE1' in list_se:
        handle_se1(mydoc)
        print('\n')

# Tell python to run main method
# main(dataset apakah {Test, Early, Middle, Late} dalam bentuk array, nomor{1,2,3,4,dst})
if __name__ == "__main__": main(0,3)